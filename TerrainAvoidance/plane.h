#ifndef PLANE_H
#define PLANE_H

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>

class Plane : public QGraphicsItem
{
public:
    Plane();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
    void advance(int phase);

private:
    qreal _angle;
    qreal _speed;
    void DoCollision();
};

#endif // PLANE_H
