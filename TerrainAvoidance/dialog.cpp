#include "dialog.h"
#include "ui_dialog.h"
#include "plane.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    // Setup GraphicScene with GrapfhiView
    SetupGraphicsView();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::paintEvent(QPaintEvent *e)
{
    //CreatePlane(1);
}

void Dialog::CreatePlane(double scale)
{
    // 1. Initialise painter
    QPainter painter(this);

    // 2. Initialise polygon
    QPolygon polygon;

    // 3. Set points
    SetPlanePoints(scale, polygon);

    // 4. Make pen
    SetupLinePen(painter);

    // 5. Make Brush
    QBrush fillBrush = SetupBrush();

    // 6. Fill the polygon
    FillPolygon(polygon, painter, fillBrush);

    // 7. Draw polygon
    painter.drawPolygon(polygon);
}

void Dialog::SetPlanePoints(double scale, QPolygon &polygon)
{
    polygon << QPoint(10 * scale, 70 * scale);
    polygon << QPoint(30 * scale, 70 * scale);
    polygon << QPoint(50 * scale, 90 * scale);
    polygon << QPoint(110 * scale, 90 * scale);
    polygon << QPoint(70 * scale, 10 * scale);
    polygon << QPoint(90 * scale, 10 * scale);
    polygon << QPoint(140 * scale, 90 * scale);
    polygon << QPoint(200 * scale, 90 * scale);
    polygon << QPoint(220 * scale, 100 * scale);
    polygon << QPoint(200 * scale, 110 * scale);
    polygon << QPoint(140 * scale, 110 * scale);
    polygon << QPoint(90 * scale, 190 * scale);
    polygon << QPoint(70 * scale, 190 * scale);
    polygon << QPoint(110 * scale, 110 * scale);
    polygon << QPoint(50 * scale, 110 * scale);
    polygon << QPoint(30 * scale, 130 * scale);
    polygon << QPoint(10 * scale, 130 * scale);
    polygon << QPoint(20 * scale, 110 * scale);
    polygon << QPoint(20 * scale, 90 * scale);
}

void Dialog::SetupLinePen(QPainter &painter)
{
    QPen linePen;
    linePen.setWidth(4);
    linePen.setColor(Qt::black);
    linePen.setJoinStyle(Qt::RoundJoin);
    painter.setPen(linePen);
}

QBrush Dialog::SetupBrush()
{
    QBrush fillBrush;
    fillBrush.setColor(Qt::blue);
    fillBrush.setStyle(Qt::SolidPattern);

    return fillBrush;
}

void Dialog::FillPolygon(QPolygon polygon, QPainter &painter, QBrush fillBrush)
{
    QPainterPath path;
    path.addPolygon(polygon);

    painter.fillPath(path, fillBrush);
}

void Dialog::CreateMountainPolygon(QBrush &fillBrush, QPolygonF &polygon, QPen &linePen)
{
    // 1. Set points of the polygon
    polygon << QPointF(1000, 500);
    polygon << QPointF(800, -50);
    polygon << QPointF(600, 400);
    polygon << QPointF(400, 200);
    polygon << QPointF(200, 450);
    polygon << QPointF(45, 150);
    polygon << QPointF(-200, 500);

    // 2. Set brush for filling the polygon with color
    fillBrush.setColor(Qt::darkGray);
    fillBrush.setStyle(Qt::SolidPattern);

    // 3. Set pen for the line
    linePen.setWidth(4);
    linePen.setColor(Qt::black);
    linePen.setJoinStyle(Qt::RoundJoin);
}

void Dialog::SetupGraphicsView()
{
    // 1. Initialise graphics view
    _scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(_scene);

    // 2. Antialiansing
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);
    _scene->setSceneRect(-200, -200, 1200, 700);

    QPen pen = QPen(Qt::red);

    // 3. Create box
    QLineF topLine(_scene->sceneRect().topLeft(), _scene->sceneRect().topRight());
    QLineF leftLine(_scene->sceneRect().topLeft(), _scene->sceneRect().bottomLeft());
    QLineF rightLine(_scene->sceneRect().topRight(), _scene->sceneRect().bottomRight());
    QLineF bottomLine(_scene->sceneRect().bottomLeft(), _scene->sceneRect().bottomRight());

    _scene->addLine(topLine, pen);
    _scene->addLine(leftLine, pen);
    _scene->addLine(rightLine, pen);
    _scene->addLine(bottomLine, pen);

    // 4. Initialise polygon elements
    QPolygonF polygon;
    QBrush fillBrush;
    QPen linePen;

    // 5. Setup mountain in graphics view
    CreateMountainPolygon(fillBrush, polygon, linePen);
    _scene->addPolygon(polygon, linePen, fillBrush);

    // 6. Setup plane
    Plane *item = new Plane;
    _scene->addItem(item);

    // 7. Create timer
    _timer = new QTimer(this);

    // 8. Timer sinals timeout do through the slot one step (advance)
    connect(_timer, SIGNAL(timeout()), _scene, SLOT(advance()));
    _timer->start(100);
}
