#include "plane.h"

Plane::Plane()
{
    // Random start rotation
    _angle = 0;
    setRotation(_angle);

    // Set speed
    _speed = 5; // 5 pixel at the time

    // Random start position
    int startX = -200;
    int startY = 40;

    setPos(mapToParent(startX, startY));
}

QRectF Plane::boundingRect() const
{
    return QRect(0, 0, 20, 20);
}

void Plane::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rect = boundingRect();
    QBrush brush(Qt::gray);
    brush.setColor(Qt::green);

    // Collision detection
    /*if(scene()->collidingItems(this).isEmpty())
    {
        // No collision
        brush.setColor(Qt::green);
    }
    else
    {
        // Collision
        brush.setColor(Qt::red);

        // Set position
        DoCollision();
    }*/

    painter->fillRect(rect, brush);
    painter->drawRect(rect);
}

void Plane::advance(int phase)
{
    if(!phase) return;

    QPointF location = this->pos();
    setPos(mapToParent(_speed, 0));
}

void Plane::DoCollision()
{
    // Change the angle with a little randomness
    if((qrand() % 1))
    {
        setRotation(rotation() + (180 + (qrand() % 10)));
    }
    else
    {
        setRotation(rotation() + (180 + (qrand() % -10)));
    }

    // See if the new position is in the bounds
    QPointF newPoint = mapToParent(-(boundingRect().width()), -(boundingRect().width() + 2));

    if(!scene()->sceneRect().contains(newPoint))
    {
        // Move it back in bounds
        newPoint = mapToParent(0, 0);
    }
    else
    {
        // Set the new position
        setPos(newPoint);
    }
}
