#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtGui>
#include <QtCore>
#include <QGraphicsScene>

namespace Ui
{
    class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

protected:
    void paintEvent(QPaintEvent *e);

private:
    Ui::Dialog *ui;
    QGraphicsPolygonItem *_polygon;
    QGraphicsScene *_scene;
    QTimer *_timer;

private: // Individual methods
    void test();
    void CreatePlane(double scale);
    void SetPlanePoints(double scale, QPolygon &polygon);
    void SetupLinePen(QPainter &painter);
    QBrush SetupBrush();
    void FillPolygon(QPolygon polygon, QPainter &painter, QBrush fillBrush);
    void CreateMountainPolygon(QBrush &fillBrush, QPolygonF &polygon, QPen &linePen);
    void SetupGraphicsView();
};

#endif // DIALOG_H
